#include <random>
#include <ctime>

std::default_random_engine salt(static_cast<unsigned long>(std::time(NULL)));

template<typename T>
T GetRandom(T min, T max)
{
    std::uniform_int_distribution<T> generator(min, max);
    return generator(salt);
}