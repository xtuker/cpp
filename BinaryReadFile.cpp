#include <fstream>
#include <string>
#include <sstream>

std::string BinaryReadFile(std::string const & fileName)
{
    std::ifstream ifd(fileName, std::ios::binary);
    if(ifd.is_open() == false)
        return std::string();
    std::ostringstream out;
    out << ifd.rdbuf();
    return out.str();
}