#include <Windows.h>
#include <string>

std::wstring StrToWStr(std::string const &str)
{
    std::wstring wstr;
    size_t contentLength = str.size();
    wstr.resize(contentLength);
    ::MultiByteToWideChar(CP_ACP, NULL, str.c_str(), static_cast<int>(str.size()), &wstr[0], static_cast<int>(contentLength));
    return wstr;
}
std::string WStrToStr(std::wstring const &wstr)
{
    std::string str;
    size_t contentLength = wstr.size();
    str.resize(contentLength);
    ::WideCharToMultiByte(CP_ACP, NULL, wstr.c_str(), static_cast<int>(wstr.size()), &str[0], static_cast<int>(contentLength), NULL, NULL);
    return str;
}