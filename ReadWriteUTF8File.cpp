#include <fstream>
#include <sstream>
#include <codecvt>
#include <clocale>

std::wstring ReadUTF8File(std::string const &filePath)
{
    const std::locale utf8_locale = std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>());
    
    std::wifstream stream(filePath);
    stream.imbue(utf8_locale);

    std::wostringstream out;
    out << stream.rdbuf();
    stream.close();

    return out.str();
}

void WriteUTF8File(std::string const &filePath, std::wstring const &text)
{
    const std::locale utf8_locale = std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>());

    std::wofstream stream(filePath, std::ios_base::binary);
    stream.imbue(utf8_locale);

    stream << L'\xFEFF'; // Write BOM header 
    stream << text;

    stream.close();
}